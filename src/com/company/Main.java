package com.company;


import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Candidate> candidates = new ArrayList<>();                         //Создание коллекции кандидатов
        for (int i = 1; i <= 10; i++) {                                        //цикл для создания 10 новых экземпляров кандидатов
            Candidate candidate;
            if (i % 2 == 0) {
                candidate = new GJJCandidate();
            } else {
                candidate = new SelfLearner();
            }
            candidate.setName("Candidate " + i);
            candidates.add(candidate);
        }
        Employer employer = new Employer();

        for (Candidate candidate : candidates) {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();
        }
    }


    public interface DescribeExperience {            //Создание интерфейса описания опыта

        void describeExperience();                //Метод для описания опыта кандидатами
    }

    public interface Hello {                    //создание интерфейса приветствия
        void hello();                           //Метод для приветствия кандидатов и работодателя
    }

    public static abstract class Human implements Hello {    //Создание абстрактного класса. Общий класс,описывающий всех участников собеседования
        private String name;                                 //инкапсуляция. Ограничение доступа к переменной извне

        public abstract void hello();                       //переопределение метода интерфейса Hello


        public void setName(String name) {                  //Сеттер переменной name
            this.name = name;
        }
    }

    public static class Employer extends Human {                //Создание класса Работодателя и наследование от класса Human
        @Override
        public void hello() {                                   //Переопределение метода интерфейса Hello
            System.out.println("hi! introduce yourself and describe your java experience please");
        }

    }

    public static abstract class Candidate extends Human implements DescribeExperience {       //Создание класса ,общего для всех кандидатов и наследование от класса Human. Имплементация иинтерфейса Описания опыта кандидатов


        @Override
        public void hello() {                                                         //Переопределение метода интерфейса Hello
            System.out.println("hi! my name is " + super.name + "!");                       //Добавление значения перемонной name в выводимый текст
        }
    }

    public static class GJJCandidate extends Candidate {                                //Создание класса Кандидата GJJ и наследование от класса Candidate

        @Override
        public void describeExperience() {                                              //Переопределение метода интерфейса describeExperience
            System.out.println("I passed successfully getJavaJob exams and code reviews.");
        }
    }

    public static class SelfLearner extends Candidate {                                  //Создание класса Кандидата самообучения и наследование от класса Candidate

        @Override
        public void describeExperience() {                                                //Переопределение метода интерфейса describeExperience
            System.out.println("I’ve been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
        }
    }
}
